CSV parser/generator
====================

[![Build Status](https://ci.gitnet.fr/api/badges/deblan/csv/status.svg)](https://ci.gitnet.fr/deblan/csv)

A simple PHP library to:

* parse a CSV file
* parse a stream as CSV datas
* generate CSV files.

PHP >= 7.1 required.

## Composer installation

```
$ composer require deblan/csv "~3"
```

Or in your composer.json:

```
{
    "require": {
        "deblan/csv": "~3"
    }
}
```

## Usages

### Generator

```php
use Deblan\Csv\Csv;

$csv = new Csv();

// Defines the delimiter (default is ;)
$csv->setDelimiter(";");

// Defines the enclosure (default is ")
$csv->setEnclosure('"');

// Defines the end of line (default is \n)
$csv->setEndOfLine("\n");

// Defines the charset (default is UTF-8)
$csv->setCharset("UTF-8");

// Add a new line at the end
$csv->addData(['Foo', '$1000'));

// Add a new line at the end
$csv->appendData(['Bar', '$600']);

// Add a new line at the beginning
$csv->prependData(['Boo', '$3000']);

// Defines all the datas
$csv->setDatas([[...], [...]]);

// Defines the header
$csv->setHeaders(['Product', 'Price']);

// Rendering
$result = $csv->render();

// Rendering to a file
$result = $csv->render('products.csv');

// Appending to a file
$result = $csv->render('products.csv', FILE_APPEND);
```

### Parse a file

```php
use Deblan\Csv\CsvParser;

$csv = new CsvParser();

// Defines the delimiter (default is ;)
$csv->setDelimiter(";");

// Defines the enclosure (default is ")
$csv->setEnclosure('"');

// Defines the end of line (default is \n)
$csv->setEndOfLine("\n");

// Headers?
$csv->setHasHeaders(true);

// Parse a file
$csv->parseFile('products.csv');

// Parse a string
$csv->parseString($myString);

// Headers and datas
$headers = $csv->getHeaders();
$products = $csv->getDatas();
```

### Parse a stream

```php
use Deblan\Csv\CsvStreamParser;

// CsvStreamParser is a CsvParser
$csv = new CsvStreamParser();

// Parse a stream
$csv->parseStream(fopen('products.csv', 'r'));

while ($data = $csv->getData()) {
	// ...
}
```
