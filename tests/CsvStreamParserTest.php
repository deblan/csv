<?php

use Deblan\Csv\CsvParser;
use PHPUnit\Framework\TestCase;
use Deblan\Csv\CsvStreamParser;

class CsvParserParserTest extends CsvParserTest
{
    public function testStreamParser()
    {
        $parser = new CsvStreamParser();
        $this->assertEquals([], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $parser = new CsvStreamParser();
        $this->expectException('\InvalidArgumentException');
        $parser->parseStream(null);
    }

    public function testStreamParser2()
    {
        $parser = new CsvStreamParser();
        $parser->setHasHeaders(true);

        $parser->parseStream(fopen(__DIR__.'/fixtures/example.csv', 'r'));
        $this->assertEquals([], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $this->assertEquals([
            'FOO',
            'BAR',
        ], $parser->getData());
        $this->assertEquals([], $parser->getDatas());
        $this->assertEquals(['FOO', 'BAR'], $parser->getHeaders());

        $this->assertEquals([
            'foo 1',
            'bar 1',
            'FOO' => 'foo 1',
            'BAR' => 'bar 1',
        ], $parser->getData());
        $this->assertEquals([
            [
                'foo 1',
                'bar 1',
                'FOO' => 'foo 1',
                'BAR' => 'bar 1',
            ],
        ], $parser->getDatas());
        $this->assertEquals(['FOO', 'BAR'], $parser->getHeaders());

        $parser->getData();
        $this->assertEquals([
            [
                'foo 1',
                'bar 1',
                'FOO' => 'foo 1',
                'BAR' => 'bar 1',
            ],
            [
                'foo 2',
                'bar 2',
                'FOO' => 'foo 2',
                'BAR' => 'bar 2',
            ],
        ], $parser->getDatas());
        $this->assertEquals(['FOO', 'BAR'], $parser->getHeaders());

        $parser->getData();
        $this->assertEquals([
            [
                'foo 1',
                'bar 1',
                'FOO' => 'foo 1',
                'BAR' => 'bar 1',
            ],
            [
                'foo 2',
                'bar 2',
                'FOO' => 'foo 2',
                'BAR' => 'bar 2',
            ],
            [
                'foo 3',
                'bar 3',
                'FOO' => 'foo 3',
                'BAR' => 'bar 3',
            ],
        ], $parser->getDatas());
        $this->assertEquals(['FOO', 'BAR'], $parser->getHeaders());
    }

    public function testStreamParser3()
    {
        $parser = new CsvStreamParser();
        $parser->setHasHeaders(false);

        $parser->parseStream(fopen(__DIR__.'/fixtures/example.csv', 'r'));
        $this->assertEquals([], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $this->assertEquals([
            'FOO',
            'BAR'
        ], $parser->getData());
        $this->assertEquals([
            [
                'FOO',
                'BAR'
            ]
        ], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $this->assertEquals([
            'foo 1',
            'bar 1',
        ], $parser->getData());
        $this->assertEquals([
            [
                'FOO',
                'BAR',
            ],
            [
                'foo 1',
                'bar 1',
            ],
        ], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $this->assertEquals([
            'foo 2',
            'bar 2',
        ], $parser->getData());
        $this->assertEquals([
            [
                'FOO',
                'BAR',
            ],
            [
                'foo 1',
                'bar 1',
            ],
            [
                'foo 2',
                'bar 2',
            ],
        ], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $this->assertEquals([
            'foo 3',
            'bar 3',
        ], $parser->getData());
        $this->assertEquals([
            [
                'FOO',
                'BAR',
            ],
            [
                'foo 1',
                'bar 1',
            ],
            [
                'foo 2',
                'bar 2',
            ],
            [
                'foo 3',
                'bar 3',
            ],
        ], $parser->getDatas());
        $this->assertEquals([], $parser->getHeaders());

        $this->assertEquals(null, $parser->getData());
    }
}
